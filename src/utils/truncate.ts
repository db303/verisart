export const truncate = (str:string, length: number) => {
    
    if(length <= 0 || str.length <= length) {
        return str;
    }
    const ending = '...';

    return str.substring(0, length) + ending;
    
};