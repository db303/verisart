import axios from 'axios';

export async function callApi(method: "post" | "get", url: string, path: string, data?: any) {
  
  try {
    const result = await axios({
      method: method,
      url: `${url}${path}`,
      data: data
    });
    return result.data;
  } catch(e) {

    return new Error(e.response.data.message);
  }

}
