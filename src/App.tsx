import React from 'react'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { Store } from 'redux'
import { History } from 'history'
import { ApplicationState } from './store'

import AppWrapper from './components/layout/AppWrapper';
import Routes from './Routes';

interface IAppProps {
    store: Store<ApplicationState>
    history: History
}
const App: React.FC<IAppProps> = ({ store, history }) => (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <AppWrapper>
                <Routes />
            </AppWrapper>
        </ConnectedRouter>
    </Provider>
);

export default App;
