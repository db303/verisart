export const fakeData = {

    id: "b8c70bc2-e2f4-400c-87cc-1a8fef30f7fc",
    user_fingerprint: "032b8abaa9e6e9eb0c71a9e21c24dc222ad375a0f280252d319d56ce4586308ba1",
    status: "confirmed",
    iri: "http://www.verisart.com/id/object/b8c70bc2-e2f4-400c-87cc-1a8fef30f7fc",
    url: "https://verisart.com/works/b8c70bc2-e2f4-400c-87cc-1a8fef30f7fc",
    hash: "0000000000000000001657d1454d880658e8c70cb49697aa89731b761cced818",
    ots_version: "0.2",
    blockchain_url: "https://www.blockchain.com/btc/address/0000000000000000001657d1454d880658e8c70cb49697aa89731b761cced818",
    private: false,
    signed: true,
    created_at: "2019-11-04T17:50:27Z",
    modified_at: "2019-11-04T17:50:27Z",
    confirmed_at: "2019-11-04T17:50:27Z",
    unit: "cm",
    width: 12.1,
    height: 12.1,
    depth: 0,
    title: "API",
    object_type: "",
    edition_volume: 21,
    edition: 1,
    inventory_number: "R3C2IHZ8",
    artist_proof: true,
    artist_proof_volume: 4,
    production_location: "London",
    production_year: 2002,
    current_location: "London",
    
    medium: "Glass-Blowing",

    artist: {
        id: "0220b957-57db-4fd5-b0a5-08ffcaf4af7c",
        name: "Leonardo Da Vinci",
        ulan_id: 500010879,
        cob: "IT",
        dob: "1452-04-15",
        website: "http://vocab.getty.edu/page/ulan/500010879",
        signature: {
            id: 420,
            url: "https://s3.amazonaws.com/verisart.public-assets/logo/verisart-logo-300.png",
            mime: "image/png",
            ext: ".png"
        }
    },
    meta: {
        background: {
            id: 420,
            url: "https://s3.amazonaws.com/verisart.public-assets/logo/verisart-logo-300.png",
            mime: "image/png",
            ext: ".png"
        },
        logo: {
            id: 420,
            url: "https://s3.amazonaws.com/verisart.public-assets/logo/verisart-logo-300.png",
            mime: "image/png",
            ext: ".png"
        },
        header: "Certificate"
    },
    collaborators_count: 4,
    history_count: 4,
    timestamps_count: 0,
    files_count: 0,
    image: "https://res.cloudinary.com/verisart/image/upload/s3/prod/images/info-objects/11_e5744e30_original.jpg"
}