import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './components/Home';
import ConnectedLogin from './components/ConnectedLogin';
import ConnectedCertificate from './components/ConnectedCertificate';


const Routes: React.FC = () => (
  <Switch>
    <Route exact path='/' component={Home} />
    <Route path='/login' component={ConnectedLogin} />
    <Route exact path='/certificate/:certificateId' component={ConnectedCertificate} />
  </Switch>
);

export default Routes;