import { certificateReducer, initialState, CertificateActionTypes } from '../../store/certificate';

describe('Certificate Reducer', () => {
    it('Should return default state', () => {
        const newState = certificateReducer(undefined, {type:''} );
        expect(newState).toEqual(initialState);
    })
    it('Should work with request', () => {
        const newState = certificateReducer(initialState, { type: CertificateActionTypes.CERTIFICATE_REQUEST, payload: initialState});
        expect (newState).toEqual({data: undefined, loading: true, errors: undefined});
    })
    it('Should work with success', () => {
        const newState = certificateReducer(initialState, { type: CertificateActionTypes.CERTIFICATE_SUCCESS, payload: 'some_token'});
        expect (newState).toEqual({data: 'some_token', loading: false, errors: undefined});
    })
    it('Should work with error', () => {
        const newState = certificateReducer(initialState, { type: CertificateActionTypes.CERTIFICATE_ERROR, payload: 'error_message'});
        expect (newState).toEqual({data: undefined, loading: false, errors: 'error_message'});
    })
});