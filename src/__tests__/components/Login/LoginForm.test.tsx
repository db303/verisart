import React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';
import LoginForm from '../../../components/Login/LoginForm';
import { AllProps } from '../../../components/ConnectedLogin';

describe('Login Form', () => {
    it('matches snapshot in not loading in state', () => {

        const auth = jest.fn();
        
        const props: AllProps = {
            token: 'some-token',
            loading: false,
            auth: auth
        }

        const renderer = ShallowRenderer.createRenderer();
        renderer.render(<LoginForm {...props} />);
        const tree = renderer.getRenderOutput();
        expect(tree).toMatchSnapshot();
    })
    it('matches snapshot in loading in state', () => {

        const auth = jest.fn();
        
        const props: AllProps = {
            token: 'some-token',
            loading: true,
            auth: auth
        }

        const renderer = ShallowRenderer.createRenderer();
        renderer.render(<LoginForm {...props} />);
        const tree = renderer.getRenderOutput();
        expect(tree).toMatchSnapshot();
    })
    it('matches snapshot in loading in error state', () => {

        const auth = jest.fn();
        
        const props: AllProps = {
            token: 'some-token',
            loading: false,
            auth: auth,
            error: 'some error'
        }

        const renderer = ShallowRenderer.createRenderer();
        renderer.render(<LoginForm {...props} />);
        const tree = renderer.getRenderOutput();
        expect(tree).toMatchSnapshot();
    })        
});