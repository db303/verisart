import React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';
import CertificateHeader from '../../../components/Certificate/Header';

import { fakeData } from '../../../__mocks__/fakeData';

describe('Certificate Header', () => {
    it('renders and matches the snapshot', () => {
        const renderer = ShallowRenderer.createRenderer();
        renderer.render(<CertificateHeader data={fakeData}/>);
        const tree = renderer.getRenderOutput();
        expect(tree).toMatchSnapshot();
    })
});