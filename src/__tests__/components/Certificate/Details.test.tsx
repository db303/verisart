import React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';
import CertificateDetails from '../../../components/Certificate/Header';

import { fakeData } from '../../../__mocks__/fakeData';

describe('Certificate Details', () => {
    it('renders and matches the snapshot', () => {
        const renderer = ShallowRenderer.createRenderer();
        renderer.render(<CertificateDetails data={fakeData}/>);
        const tree = renderer.getRenderOutput();
        expect(tree).toMatchSnapshot();

    })
});