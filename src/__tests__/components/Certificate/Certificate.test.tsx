import React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';
import Certificate from '../../../components/Certificate/Certificate';

import { fakeData } from '../../../__mocks__/fakeData';

describe('Certificate', () => {
    it('renders and matches the snapshot', () => {
        const renderer = ShallowRenderer.createRenderer();
        renderer.render(<Certificate data={fakeData}/>);
        const tree = renderer.getRenderOutput();
        expect(tree).toMatchSnapshot();

    })
});
