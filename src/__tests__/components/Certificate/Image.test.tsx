import React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';
import CertificateImage from '../../../components/Certificate/Image';

import { fakeData } from '../../../__mocks__/fakeData';

describe('Certificate Image', () => {
    it('renders and matches the snapshot', () => {
        const renderer = ShallowRenderer.createRenderer();
        renderer.render(<CertificateImage src={fakeData.image} />);
        const tree = renderer.getRenderOutput();
        expect(tree).toMatchSnapshot();
    })
});