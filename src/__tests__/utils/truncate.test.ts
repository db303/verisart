import { truncate } from '../../utils/truncate';

describe('truncate function', () => {
    it('returns same string if sting length is greater than truncate length', () =>{
        expect(truncate('', 0)).toEqual('')
        expect(truncate('a', 1)).toEqual('a')
        expect(truncate('a', 2)).toEqual('a')
        expect(truncate('aa', 3)).toEqual('aa')
    })
    it('returns truncated string with adds 3 dots in the end if string length is greater than truncate val', () => {
        expect(truncate('aa', 1)).toEqual('a...')
        expect(truncate('aaa', 2)).toEqual('aa...')
        expect(truncate('aaaadsdsdsdkkkklllssssss', 10)).toEqual('aaaadsdsds...')
        expect(truncate('aaaa  dsdsdkkkklllssssss', 10)).toEqual('aaaa  dsds...')
    })
})