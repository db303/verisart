export interface ICertificate {
  id: string,
  user_fingerprint: string,
  status: string,
  iri: string,
  url: string,
  hash: string,
  ots_version: string,
  blockchain_url: string,
  private: boolean,
  signed: boolean,
  created_at: string,
  modified_at: string,
  confirmed_at: string,
  unit: string,
  width: number,
  height: number,
  depth: number,
  title: string,
  object_type: string,
  edition_volume: number,
  edition: number,
  inventory_number: string,
  artist_proof: boolean,
  artist_proof_volume: number,
  production_location: string,
  production_year: number,
  current_location: string,
  notes?: Note[]
  medium: string,
  tags?: Tag[],
  representations?: string[],
  artist: Artist,
  transferred?: Transferred,
  endorsements?: Endorsement[],
  documents?: Document[],
  owner?: Owner,
  history?: History[],
  meta: Meta,
  collaborators_count: number,
  history_count: number,
  timestamps_count: number,
  files_count: number,
  image: string
}

export interface Note {
  date: string,
  data: string,
  user: User
}

export interface User {
  id: number,
  fingerprint: string
}

export interface Tag {
  id: number,
  type: string,
  name: string,
}

export interface Artist {
  id: string,
  name: string,
  ulan_id: number,
  cob: string,
  dob: string,
  website: string,
  signature: Signature
}
          
export interface Signature {
  id: number,
  url: string,
  mime: string,
  ext: string
}
export interface Transferred {
  id: string,
  from: string,
  to: string,
  status: string,
  created_at: string,
  modified_at: string
} 
export interface Endorsement {
  from: number,
  email: string,
  date: string,
  private: boolean,
  status: string
}           
export interface Document {
  type: string,
  media: Media[]
}         
export interface Media {
  id: number,
  url: string,
  mime: string,
  ext: string
}
export interface Owner {
  id: number,
  name: string
}
export interface History {
  id: number,
  date: string,
  type: string,
  User: User,
  data: HistoryData
}
export interface HistoryData {
  address: string,
  address_blockchain_url: string,
  hash: string,
  transaction: string,
  transaction_blockchain_url: string;
  files: string[]
}
export interface Meta {
  background: Media,
  logo: Media,
  header: string,

}

export type ApiResponse = Record<string, any>
  
export enum CertificateActionTypes {
    CERTIFICATE_REQUEST = '@@certificate/CERTIFICATE_REQUEST',
    CERTIFICATE_SUCCESS = '@@certificate/CERTIFICATE_SUCCESS',
    CERTIFICATE_ERROR = '@@certificate/CERTIFICATE_ERROR',
}

export interface CertificateState {
    readonly loading: boolean
    readonly data?: ICertificate | undefined 
    readonly errors?: string
}
