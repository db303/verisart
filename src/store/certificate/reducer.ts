import { Reducer } from 'redux'
import { CertificateState, CertificateActionTypes } from './types'

export const initialState: CertificateState = {
  data: undefined,
  errors: undefined,
  loading: false
}

export const fetchCertificate = (certificateId: string) => ({
    type: CertificateActionTypes.CERTIFICATE_REQUEST,
    payload: { certificateId }
  });


const reducer: Reducer<CertificateState> = (state = initialState, {type, payload} ) => {
    
    switch (type) {
        case CertificateActionTypes.CERTIFICATE_REQUEST: {
          return { ...state, data: undefined, loading: true, errors: undefined }
        }
        case CertificateActionTypes.CERTIFICATE_SUCCESS: {
            return { ...state, data: payload, loading: false };
        }
        case CertificateActionTypes.CERTIFICATE_ERROR: {
            return { ...state, data: undefined, errors: payload, loading: false }
        }
        default:
            return state;
    }
};

export { reducer as certificateReducer }
