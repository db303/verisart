import { action } from 'typesafe-actions'
import { CertificateActionTypes, ICertificate } from './types'

export const certificateRequest = (certificateId: string) => action(CertificateActionTypes.CERTIFICATE_REQUEST, { certificateId })
export const certificateSuccess = (certificate: ICertificate) => action(CertificateActionTypes.CERTIFICATE_SUCCESS, certificate)
export const certificateError = (message: string) => action(CertificateActionTypes.CERTIFICATE_ERROR, message)