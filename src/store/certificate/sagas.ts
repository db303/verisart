import { all, call, fork, put, takeLatest } from 'redux-saga/effects'
import { CertificateActionTypes } from './types'
import { certificateError, certificateSuccess } from './actions'
import { callApi } from '../../utils/api';

const API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT || ''

function* fetchCertificate({ payload: { certificateId }}:any) {
  try {
    const res = yield call(callApi, 'get', API_ENDPOINT, `/certificates/${certificateId}`)

    if (res instanceof Error) {
      yield put(certificateError(res.message));
    } else {
      yield put(certificateSuccess(res))
    }
  } catch (err) {
    if (err instanceof Error) {
      yield put(certificateError(err.stack!))
    } else {
      yield put(certificateError('An unknown error occured.'))
    }
  }
}

function* watchCertificateRequest() {
  yield takeLatest(CertificateActionTypes.CERTIFICATE_REQUEST, fetchCertificate)
}

export function* certificateSaga() {
  yield all([fork(watchCertificateRequest)])
}