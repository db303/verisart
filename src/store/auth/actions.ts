import { action } from 'typesafe-actions'
import { AuthActionTypes} from './types'

export const authRequest = (email: string, password: string) => action(AuthActionTypes.AUTH_REQUEST, {email, password})
export const authSuccess = (token: string) => action(AuthActionTypes.AUTH_SUCCESS, token)
export const authError = (message: string) => action(AuthActionTypes.AUTH_ERROR, message)