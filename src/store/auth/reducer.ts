import { Reducer } from 'redux'
import { AuthState, AuthActionTypes } from './types'

const initialState: AuthState = {
  token: localStorage.getItem('token') || '',
  errors: undefined,
  loading: false
}

export const authorize = (email: string, password: string) => ({
    type: AuthActionTypes.AUTH_REQUEST,
    payload: { email, password }
  });


const reducer: Reducer<AuthState> = (state = initialState, { type, payload }:any ) => {
    
    switch (type) {
        case AuthActionTypes.AUTH_REQUEST: {
          return { ...state, loading: true }
        }
        case AuthActionTypes.AUTH_SUCCESS: {
            return { ...state, token: payload, errors: undefined, loading: false };
        }
        case AuthActionTypes.AUTH_ERROR: {
          
            return { ...state, token: '', errors: payload, loading: false }
        }
        default:
            return state;
  }
};
  

export { reducer as authReducer }
