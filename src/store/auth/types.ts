  export type ApiResponse = Record<string, any>
  
export enum AuthActionTypes {
    AUTH_REQUEST = '@@auth/AUTH_REQUEST',
    AUTH_SUCCESS = '@@auth/AUTH_SUCCESS',
    AUTH_ERROR = '@@auth/AUTH_ERROR',
}

export interface AuthState {
    readonly loading: boolean
    readonly token: string
    readonly errors?: string
    
}