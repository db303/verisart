import { all, call, fork, put, takeLatest } from 'redux-saga/effects'
import { AuthActionTypes } from './types'
import { authError, authSuccess } from './actions'
import { callApi } from '../../utils/api';

const API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT || ''

function* authorize({ payload: { email, password }}:any) {
  try {
    const res = yield call(callApi, 'post', API_ENDPOINT, '/login', { email, password})

    if (res instanceof Error) {
      yield put(authError(res.message));
      localStorage.removeItem('token');
    } else {
      yield put(authSuccess(res.token))
      localStorage.setItem('token', res.token);
    }
  } catch (err) {
    if (err instanceof Error) {
      yield put(authError(err.stack!))
    } else {
      yield put(authError('An unknown error occured.'))
    }
  }
}

function* watchAuthRequest() {
  yield takeLatest(AuthActionTypes.AUTH_REQUEST, authorize)
}

export function* authSaga() {
  yield all([fork(watchAuthRequest)])
}