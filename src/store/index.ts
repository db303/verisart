import { combineReducers } from 'redux'
import { all, fork } from 'redux-saga/effects'
import { connectRouter, RouterState } from 'connected-react-router'
import { History } from 'history';

import { AuthState, authReducer, authSaga } from './auth';
import { CertificateState, certificateReducer, certificateSaga } from './certificate';

// The top-level state object.
export interface ApplicationState {
  auth: AuthState
  certificate: CertificateState
  router: RouterState
}

// Whenever an action is dispatched, Redux will update each top-level application state property
// using the reducer with the matching name. It's important that the names match exactly, and that
// the reducer acts on the corresponding ApplicationState property type.
export const rootReducer = (history: History) => combineReducers<ApplicationState>({
  auth: authReducer,
  certificate: certificateReducer,
  router: connectRouter(history),
})

export function* rootSaga() {
    yield all([fork(authSaga), fork(certificateSaga)])
  }