import React from 'react';
import styled from 'styled-components';
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { ApplicationState } from '../store'
import { authorize } from '../store/auth';
import { Redirect } from 'react-router-dom';

import LoginForm from './Login/LoginForm';

// Separate state props + dispatch props to their own interfaces.
interface PropsFromState {
    loading: boolean
    token: string
    error?: string
}
  
interface PropsFromDispatch {
    auth: typeof authorize
}

export type AllProps = PropsFromState & PropsFromDispatch


const Login: React.FC<AllProps> = (props) => {
    
    if(props.token) {
        return <Redirect to='/' />
    }

    return (
        <>
            <SmallTitle style={{maxWidth: 350}}>login</SmallTitle>
            <Content style={{maxWidth: 350}}>
                <Wrapper>
                    <LoginFormContent>
                        <LoginForm {...props}  />
                    </LoginFormContent>
                </Wrapper>
            </Content>
        </>
    )
}

const mapStateToProps = ({ auth }: ApplicationState) => ({
    loading: auth.loading,
    error: auth.errors,
    token: auth.token
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
    auth: (email:string, password: string) => dispatch(authorize(email,password))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login)


const SmallTitle = styled.div`
    margin-top: 0px;
    padding-top: 70px;
    font-weight: normal;
    text-transform: uppercase;
    font-family: "SimplonNorm-Bold", Helvetica, sans-serif;
    font-size: 18px;
    line-height: 27px;
    letter-spacing: 0.125em;
    margin: 15px 0px;
    margin-top: 15px;
    width: 100%;
`;

const Content = styled.div`
    width: 100%;
    text-align: center;
    background: white none repeat scroll 0% 0%;
    position: relative;
    margin: 5px;
    padding: 15px;
`;

const Wrapper = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
`;

const LoginFormContent = styled.div`
    display: flex;
    flex-direction: column;
    padding: 0px;
    width: 100%;
`;