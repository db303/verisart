import React from 'react';
import useForm from 'react-hook-form';
import styled from 'styled-components';
import { AllProps } from '../ConnectedLogin';

type LoginFormData = {
    email: string;
    password: string;
};

const LoginForm: React.FC<AllProps> = (props) => {
    
    const { handleSubmit, register, errors } = useForm<LoginFormData>();  
    const  {loading, error, auth} = props;

    const onSubmit = async ( values: LoginFormData ) => {
        auth(values.email, values.password);
    };

    return (
        <LoginWithEmailForm onSubmit={handleSubmit(onSubmit)}>
            <TextInputWrapper>
                <TextInput
                    hasError={errors.email ? true : false}
                    name="email"
                    placeholder="email"
                    ref={register({
                        pattern: {
                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                            message: "Invalid email address"
                        }
                    })}
                    />
            </TextInputWrapper>
            { errors.email && <ErrorFieldMessages>{ errors.email.message }</ErrorFieldMessages>}
            <Spacing>
                <TextInputWrapper>
                    <TextInput
                        hasError={errors.password ? true : false}
                        name="password"
                        placeholder="enter password"
                        ref={register({
                            pattern: {
                                value: /^.{8,}$/,
                                message: "Password must be 8 characters long"
                            }
                        })}
                        />
                </TextInputWrapper>
                { errors.password && <ErrorFieldMessages>{ errors.password.message }</ErrorFieldMessages>}
            </Spacing>
            { error && <p>{error}</p> }
            <Spacing>
                <Button>{loading ? 'loging in' : 'login'}</Button>
            </Spacing>
        </LoginWithEmailForm>
    );
  };

  interface ITextInputProps {
    hasError?: boolean;
}

export default LoginForm;

const TextInput = styled('input')<ITextInputProps>`
    border: 1px solid ${props  => props.hasError ? 'rgb(208, 2, 27)' : 'rgb(220, 220, 220)'}; 
    position: relative;
    padding: 10px 15px;
    width: 100%;
    outline: currentcolor none medium;
    font-family: "FreightMedium", Georgia, sans-serif;
    font-size: 14px;
    line-height: 21px;
    text-transform: none;
    ::placeholder {
        text-transform: uppercase;
        color: ${props  => props.hasError ? 'rgb(208, 2, 27)' : 'rgb(220, 220, 220)'};
        font-family: "SimplonNorm-Medium", Helvetica, sans-serif;
        font-size: 14px;
        line-height: 21px;
        letter-spacing: 0.125em;
    }
`;

const TextInputWrapper = styled.div`
    display: flex;
    width: 100%;
    flex-direction: column;
    align-items: flex-start;
    position: relative;
    overflow: visible;
`;

const LoginWithEmailForm = styled.form`
    padding-bottom: 0px;
    flex: 1 1 0%;
`;

const Button = styled.button`
    outline: currentcolor none medium;
    display: flex;
    justify-content: center;
    align-items: center;
    font-family: "SimplonNorm-Bold", Helvetica, sans-serif;
    font-size: 14px;
    line-height: 21px;
    letter-spacing: 0.125em;
    text-transform: uppercase;
    font-weight: normal;
    text-decoration: none;
    transition: all 250ms ease-in-out 0s;
    border: 1px solid black;
    color: black;
    width: 100%;
    padding: 15px 0px;
    :hover {
        background-color: black;
        cursor: pointer;
        color: white;
    }
`;

const Spacing = styled.div`
    flex: 1 1 0%;
    align-items: flex-start;
    margin-top: 15px;
`;

const ErrorFieldMessages = styled.div`
    color: rgb(208, 2, 27);
    margin-top: 5px;
    font-family: "FreightTextProBook-Italic", Georgia, sans-serif;
    font-size: 14px;
    line-height: 21px;
    text-align: left;
`;