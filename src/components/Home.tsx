import React from 'react';
import { Link } from 'react-router-dom';

const Home: React.FC = () => (  
    <Link to="/certificate/b8c70bc2-e2f4-400c-87cc-1a8fef30f7fc">View certificate</Link>
);

export default Home;