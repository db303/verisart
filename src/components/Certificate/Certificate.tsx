import React from 'react';
import styled from 'styled-components';
import Details from './Details';
import Header from './Header';
import Image from './Image';

import { ICertificate } from '../../store/certificate';

interface ICertificateProps {
    data: ICertificate
}

const Certificate:React.FC<ICertificateProps> = ({ data }) => (
    <CertificatePreviewWrapper>
    <BackgroundWrapper />
      <CertificatePreviewOverlay>
          <Header data={data} />
          <CertificatePreviewContent>
              <CertificatePreviewColumn>
                  <Image src={data.image} />
              </CertificatePreviewColumn>
              <CertificatePreviewColumn style={{marginLeft: 30}}>
                  <Details data={data} />
              </CertificatePreviewColumn>
          </CertificatePreviewContent>    
      </CertificatePreviewOverlay>
  </CertificatePreviewWrapper>
)

export default Certificate;

const BackgroundWrapper = styled.div`    
    position: absolute;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
    z-index: 0;
    transform-origin: center center 0px;
    background-image: url("https://res.cloudinary.com/verisart/image/upload/w_1024,h_768/s3/prod/images/partner-cert-logos/7740_d48c46c8_original.jpg");
    background-size: cover;
    background-position: center center;
    opacity: 0.4;
`;

const CertificatePreviewOverlay = styled.div`    
    flex-direction: column;
    display: flex;
    position: relative;
    height: 100%;
    flex: 1 1 0%;
`;

const CertificatePreviewWrapper = styled.div`
    display: flex;
    flex-direction: column;
    position: relative;
    overflow: hidden;
    min-height: 700px;
    margin: 15px;
`;

const CertificatePreviewContent = styled.div`
    flex-direction: row;
    margin: 0px 40px 40px;
    max-width: 100%;
    flex: 1 1 0%;
    display: flex;
`;

const CertificatePreviewColumn = styled.div`
    flex: 1 0 0%;
    max-width: 50%;
    margin: 0px;
    -moz-box-pack: center;
    justify-content: center;
    -moz-box-align: center;
    align-items: center;
    display:flex;
    background-color: white;
`;