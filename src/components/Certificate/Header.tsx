import React from 'react';
import styled from 'styled-components';
import Moment from 'react-moment';
import { ICertificate } from '../../store/certificate';

interface IHeader {
    data: ICertificate
}

const Header:React.FC<IHeader> = ( { data }) => {
    
    return (
        <HeaderWrapper>
            <HeaderSection style={{flex: '1 1 0%'}}>
                <HeaderLogo />
                <HeaderTitle>{data.meta.header}</HeaderTitle>
            </HeaderSection>
            <HeaderSection>
                <HeaderDisclaimer 
                    timestamps={data.timestamps_count}
                    collaborators={data.collaborators_count}
                    files={data.files_count}
                />
                <HeaderStatus 
                    status={data.status}
                    confirmed_at={data.confirmed_at}
                />    
            </HeaderSection>
        </HeaderWrapper>
    )
}

const HeaderLogo:React.FC = () => (
    <StyledHeaderLogo>
        <img src="https://res.cloudinary.com/verisart/image/upload/w_1024,h_768/s3/prod/images/partner-cert-logos/7739_d48c46c8_original.jpg" 
            alt="logo" />
    </StyledHeaderLogo>
);

interface IHeaderStatus {
    status: string,
    confirmed_at: string
}
const HeaderStatus:React.FC<IHeaderStatus> = ({ status, confirmed_at }) => (
    <StyledHeaderStatus>
        <div><Moment format="MMM DD YYYY hh:mm:ss UTC">{confirmed_at}</Moment></div>
        <StyledHeaderState>{status}</StyledHeaderState>
    </StyledHeaderStatus>
);

interface IHeaderDisclaimer {
    timestamps: number,
    files: number,
    collaborators: number,
}

const HeaderDisclaimer:React.FC<IHeaderDisclaimer> = ({ timestamps, files, collaborators}) => (
    <StyledHeaderDisclaimer>
        <HeaderMeta>
            <DetailedMetaItemWrapper>
                <DetailsMetaItemLabel>Timestamps:</DetailsMetaItemLabel>
                <DetailsMetaItemValue><div>{timestamps}</div></DetailsMetaItemValue>
            </DetailedMetaItemWrapper>

            <DetailedMetaItemWrapper>
                <DetailsMetaItemLabel>Files:</DetailsMetaItemLabel>
                <DetailsMetaItemValue><div>{files}</div></DetailsMetaItemValue>
            </DetailedMetaItemWrapper>
            
            <DetailedMetaItemWrapper>
                <DetailsMetaItemLabel>Collaborators:</DetailsMetaItemLabel>
                <DetailsMetaItemValue><div>{collaborators}</div></DetailsMetaItemValue>
            </DetailedMetaItemWrapper>    
        
        </HeaderMeta>    
    </StyledHeaderDisclaimer>
);

const HeaderWrapper = styled.div`
    flex-direction: row;
    margin: 40px 40px 30px;
    min-height: 90px;
    display: flex;
    background-color: white;
`;

const HeaderSection = styled.div`
    display: flex;
`;

const StyledHeaderLogo = styled.div`
    margin: 0px;
    flex: 0 1 0%;
    display: flex;
    justify-content: center;
    align-items: center;
    img {
        width: 150px;
        height: 112.5px;
    }
`;

const StyledHeaderStatus = styled.div`
    display: flex;
    width: 80px;
    flex-direction: column;
    -moz-box-pack: center;
    justify-content: center;
    align-items: flex-start;
    font-family: "SimplonNorm-Bold", Helvetica, sans-serif;
    font-size: 10px;
    line-height: 15px;
    letter-spacing: 0.125em;
    text-transform: uppercase;
    font-weight: normal;
    text-align: left;
    margin-right: 15px;
    div {
        font-family: "SimplonNorm-Bold", Helvetica, sans-serif;
        font-size: 10px;
        line-height: 15px;
        letter-spacing: 0.125em;
        text-transform: uppercase;
        font-weight: normal;
        text-align: left;
    }
`;

const StyledHeaderState = styled.div`
    color: rgb(0, 178, 66);
    text-transform: uppercase;
    margin-top: 2px;
    text-align: left;
`

const HeaderTitle = styled.div`
    margin-top: 0px;
    padding-left: 40px;
    font-family: "SimplonNorm-Bold", Helvetica, sans-serif;
    font-size: 14px;
    line-height: 21px;
    letter-spacing: 0.125em;
    text-transform: uppercase;
    font-weight: normal;
    display: flex;
    -moz-box-pack: start;
    justify-content: flex-start;
    -moz-box-align: center;
    align-items: center;
    flex: 1 1 0%;
    padding: 10px 10px 10px 20px;
    text-align: left;
`;

const StyledHeaderDisclaimer = styled.div`
    padding: 22px 10px;
    align-items: flex-end;
    margin-right: 10px;
    flex: 1 1 0%;
`;

const HeaderMeta = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    align-items: flex-start;
    -moz-box-pack: center;
    justify-content: center;
    flex-direction: column;
`;

const DetailedMetaItemWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    -moz-box-pack: start;
    justify-content: flex-start;
    font-family: "SimplonNorm-Bold", Helvetica, sans-serif;
    font-size: 10px;
    line-height: 15px;
    letter-spacing: 0.125em;
    text-transform: uppercase;
    font-weight: normal;
`;

const DetailsMetaItemLabel = styled.div`
    color: black;
    display: flex;
    -moz-box-pack: center;
    justify-content: center;
    -moz-box-align: center;
    align-items: center;
`;

const DetailsMetaItemValue = styled.div`
    display: flex;
    -moz-box-pack: center;
    justify-content: center;
    -moz-box-align: center;
    align-items: center;
    word-break: break-all;
    text-align: left;
    margin-left: 5px;
`;

export default Header;