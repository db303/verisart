import React from 'react';
import styled from 'styled-components';

interface IImage {
    src: string
}
const Image:React.FC<IImage> = ({ src }) => (
    <StyledImage src={src} alt={''} />
);

export default Image;

const StyledImage = styled.img`
    height: auto;
    width: auto;
    max-height: 500px;
    padding: 20px;
    flex: 1 1 0%;
    max-width: 100%;
`;