import React from 'react';
import styled from 'styled-components';
import { ICertificate } from '../../store/certificate';
import { truncate } from '../../utils/truncate';

interface IDetails {
    data: ICertificate
}
const Details: React.FC<IDetails> = ({ data }) => (
    <DetailsWrapper>
        <DetailsHeader>
            <DetailsTitle>{data.artist.name}</DetailsTitle>
        </DetailsHeader>
        
        <DetailsContent>
            <DetailsItemWrapper>
                <DetailsItemLabel><pre>Title</pre></DetailsItemLabel>
                <DetailsItemValue>{data.title}</DetailsItemValue>
            </DetailsItemWrapper>
            <DetailsItemWrapper>
                <DetailsItemLabel><pre>Production Date</pre></DetailsItemLabel>
                <DetailsItemValue>{data.production_year}</DetailsItemValue>
            </DetailsItemWrapper>
            <DetailsItemWrapper>
                <DetailsItemLabel><pre>Edition</pre></DetailsItemLabel>
                <DetailsItemValue>{data.edition}</DetailsItemValue>
            </DetailsItemWrapper>
            <DetailsItemWrapper>
                <DetailsItemLabel><pre>Volume</pre></DetailsItemLabel>
                <DetailsItemValue>{data.edition_volume}</DetailsItemValue>
            </DetailsItemWrapper>
            <DetailsItemWrapper>
                <DetailsItemLabel><pre>Medium</pre></DetailsItemLabel>
                <DetailsItemValue>{data.medium}</DetailsItemValue>
            </DetailsItemWrapper>
            <DetailsItemWrapper>
                <DetailsItemLabel><pre>Dimensions (H x W)</pre></DetailsItemLabel>
                <DetailsItemValue>{data.height} x {data.width} {data.unit}</DetailsItemValue>
            </DetailsItemWrapper>
            <DetailsItemWrapper>
                <DetailsItemLabel><pre>Current Owner</pre></DetailsItemLabel>
                <DetailsItemValue>{truncate(data.user_fingerprint, 30)}</DetailsItemValue>
            </DetailsItemWrapper>
            <DetailsItemWrapper>
                <DetailsItemLabel><pre>URL</pre></DetailsItemLabel>
                <DetailsItemValue><a href={data.url} target="_new">{truncate(data.url, 30)}</a></DetailsItemValue>
            </DetailsItemWrapper>
            <DetailsItemWrapper>
                <DetailsItemLabel><pre>Blockchain Address</pre></DetailsItemLabel>
                <DetailsItemValue><a href={"https://www.blockchain.com/btc/block/" + data.hash} target="_new">{truncate(data.hash, 30)}</a></DetailsItemValue>
            </DetailsItemWrapper>
        </DetailsContent>
            
        <DetailsFooter>
            <DetailsExtra>
                <DetailsExtraContent>
                    <DetailsTitle>Verified by Verisart</DetailsTitle>
                </DetailsExtraContent>
                <DetailsQRCode src="https://verisart.com/api/v2/public/certificates/f7dc6685-d4a0-403d-ab1c-46b9c8ffb4b3/qr" alt="" />
            </DetailsExtra>
        </DetailsFooter>
    </DetailsWrapper>
)

export default Details;


const DetailsWrapper = styled.div`
    display: flex;
    background-color: white;
    flex: 1 1 0%;
    flex-direction: column;
    height: 100%;
    max-width: 100%;
`;

const DetailsHeader = styled.div`
    display: flex;
    -moz-box-pack: center;
    min-height: 100px;
`;

const DetailsTitle = styled.div`
    display: flex;
    flex: 1 1 0%;
    padding: 20px;
    text-align: left;
    overflow-wrap: break-word;
    white-space: normal;
    word-break: break-all;
    font-family: "SimplonNorm-Bold", Helvetica, sans-serif;
    font-size: 14px;
    line-height: 21px;
    letter-spacing: 0.125em;
    text-transform: uppercase;
    font-weight: normal;
`;

const DetailsContent = styled.div`
    display: flex;
    flex-direction: column;
    margin: 0px 20px;
    border-top: 1px solid rgb(220, 220, 220);
    flex: 1 1 0%;
    -moz-box-pack: start;
    justify-content: flex-start;
    align-items: flex-start;
`;

const DetailsFooter = styled.div`
    display: flex;
    flex-direction: column;
    margin: 20px 0px;
    -moz-box-pack: end;
    justify-content: flex-end;
    height: 130px;
`;

const DetailsExtra = styled.div`
    display: flex;
    width: 100%;
    flex-direction: row;
    align-items: flex-end;
    -moz-box-pack: end;
    justify-content: flex-end;
    padding: 0px 20px;
`;

const DetailsExtraContent = styled.div`
    flex: 1 1 0%;
`;


const DetailsQRCode = styled.img`
    height: 80px;
    width: 80px;
`;

const DetailsItemWrapper = styled.div`
    border-bottom: 1px solid rgb(220, 220, 220);
    padding: 10px;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    -moz-box-pack: start;
    justify-content: flex-start;
    max-width: 100%;
    width: 100%;
`;
const DetailsItemLabel = styled.div`
    width: 110px;
    height: 100%;
    display: flex;
    -moz-box-pack: start;
    justify-content: flex-start;
    align-items: flex-start;
    pre {
        font-family: "FreightMediumItalic", Georgia, sans-serif;
        font-size: 12px;
        text-align: left;
        height: 100%;
        width: 100%;
        padding: 0px;
        margin: 0px;
    }

`;
const DetailsItemValue = styled.div`
    flex: 1 1 0%;
    display: flex;
    -moz-box-pack: start;
    justify-content: flex-start;
    align-items: flex-start;
    overflow-wrap: break-word;
    font-family: "SimplonNorm-Medium", Helvetica, sans-serif;
    font-size: 12px;
    line-height: 18px;
    letter-spacing: 0.125em;
    text-transform: uppercase;
    font-weight: normal;
    text-align: left;
    a {
        text-transform: none;
        text-align: left;
        text-decoration: none;
        color: black;
        word-break: break-all;
        
    }
    a:hover {
        outline-width: 0;
    }
`;