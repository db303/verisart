import React, {useCallback, useEffect} from 'react';
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router';
import styled from 'styled-components'

import { ApplicationState } from '../store'
import { ICertificate, fetchCertificate } from '../store/certificate';
import Certificate from './Certificate/Certificate';


interface PropsFromState {
    loading: boolean
    data: ICertificate
    errors?: string
}
  
interface PropsFromDispatch {
    fetchCertificate: typeof fetchCertificate
}

type AllProps = PropsFromState & PropsFromDispatch & RouteComponentProps<{certificateId: string}>


const ConnectedCertificate:React.FC<AllProps> = (props) => {

    const  {loading, errors, data, fetchCertificate, match:{ params: { certificateId }} } = props; 
    
    const fetchCert = useCallback(() => {
        fetchCertificate(certificateId)  
    }, [fetchCertificate, certificateId]) 

    useEffect(() => {
        if(!data || data.id !== certificateId) { fetchCert(); }
    }, [fetchCert, data, certificateId]);

    if(errors === '') { return <p>Not able to fetch certificate {certificateId} </p>}   
    if(loading) { return <p>Please wait, fetching certificate data..</p>}
    if(!data) { return(<p></p>) }
   
    return ( 
        <Width>
          <Content>
            <Certificate data={data} />
          </Content>
        </Width>
    )
}

const mapStateToProps = ({ certificate }: ApplicationState) => ({
    loading: certificate.loading,
    errors: certificate.errors,
    data: certificate.data  
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
    fetchCertificate: (certificateId: string) => dispatch(fetchCertificate(certificateId))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
    // @ts-ignore
)(ConnectedCertificate);


const Width = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    max-width: 100%;
    width: 1024px;
`;

const Content = styled.div`
    width: 100%;
    text-align: center;
    flex: 1 1 0%;
    max-width: none;
    background: white none repeat scroll 0% 0%;
    position: relative;
    margin: 5px;
`;