import React from 'react';
import styled from 'styled-components';

import Header from './Header';
import AppMain from './AppMain';

interface IAppWrapper {
    children: JSX.Element;
}

const AppWrapper: React.FC<IAppWrapper> = ({ children }) => (
    <StyledAppWrapper>
        <Header />
        <AppMain>
            { children }
        </AppMain>
    </StyledAppWrapper>
);

const StyledAppWrapper = styled.div`
    display: flex;
    min-width: 100%;
    min-height: 100%;
    flex-direction: column;
`;

export default AppWrapper;