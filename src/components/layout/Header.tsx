import React from 'react';
import Wrapper from './Wrapper';
import Content from './Content';
import Logo from './Logo';
import Navigation from './Navigation';


const Header: React.FC = () => (
    <Wrapper>
        <Content>
            <>
            <Logo />
            <Navigation />
            </>
        </Content>
    </Wrapper>
);

export default Header;

