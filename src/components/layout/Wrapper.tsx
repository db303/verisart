import React from 'react';
import styled from 'styled-components';

interface IWraper {
    children: JSX.Element;
}

const Wrapper: React.FC<IWraper> = ({ children }) => (
    <StyledWrapper>
        { children }
    </StyledWrapper>    
);

const StyledWrapper = styled.div`
    display: flex;
    background-color: black;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`;

export default Wrapper;