import React from 'react';
import styled from 'styled-components';

interface IContent {
    children: JSX.Element;
}

const Content: React.FC<IContent> = ({ children }) => (
    <StyledContent>
        { children }
    </StyledContent>
); 

const StyledContent = styled.div`
    flex: 1 1 0%;
    height: 70px;
    min-height: 70px;
    width: 70%;
    max-width: 1024px;
    display: flex;
    flex-direction: row;
    align-items: center;
`;

export default Content;