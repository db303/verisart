import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import logo from '../../assets/logo.svg';

const Logo: React.FC = () => (
    <LogoWrapper>
        <Link to='/'>
            <StyledLogo src={logo} alt="verisart" />
        </Link>
    </LogoWrapper>
);

const StyledLogo = styled.img`
    width: 205px;
    height: auto;
    margin-left: 15px;
`;

const LogoWrapper = styled.div`
    flex: 1 1 0%;
    justify-content: center;
    align-items: flex-start;
`;

export default Logo;