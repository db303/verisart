import React from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

const menuItems = [
    {
        title: 'news',
        path: '/'
    },
    {
        title: 'partners',
        path: '/'
    },
    {
        title: 'join',
        path: '/'
    },
    {
        title: 'log in',
        path: '/login'
    },
];

const Navigation:React.FC = () => (
    <StyledNav>
        {
            menuItems.map((item, idx) => (
                <StyledNavItem key={idx}>
                    <NavLink to={item.path}>{ item.title }</NavLink>
                </StyledNavItem>
            ))
        }
    </StyledNav>
);

const StyledNav = styled.nav`
    font-family: "SimplonNorm-Medium", Helvetica, sans-serif;
    box-shadow: none;
    animation: 0s ease 0s 1 normal none running none;
    display: flex;
    background-color: transparent;
    letter-spacing: 1.5px;
    line-height: 22.5px;
    justify-content: flex-end;
    align-items: center;
    position: relative;
    flex-direction: row;
    font-size: 15px;
`;

const StyledNavItem = styled.div`
    padding: 0px;
    border-bottom: medium none;
    color: white;
    display: flex;
    align-items: center;
    margin: 0px 15px;
    text-transform: uppercase;
    a:link,
    a:visited,
    a:hover
    {
        text-decoration: none;
        color: white;
    }
`;

export default Navigation;