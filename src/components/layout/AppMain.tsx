import React from 'react';
import styled from 'styled-components';

interface IAppMain {
    children: JSX.Element;
}

const AppMain: React.FC<IAppMain> = ({ children }) => (
    <StyledAppMain>
        <StyledPage>
            { children }
        </StyledPage>
    </StyledAppMain>
);

const StyledAppMain = styled.div`
    flex: 1 1 0%;
`;

const StyledPage = styled.div`
    width: auto;
    padding: 10px;
    display: flex;
    flex-direction: column;
    align-items: center;
`;

export default AppMain;
